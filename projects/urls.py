from django.urls import path
from projects.views import projects, project_details, create_project


urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>/", project_details, name="show_project"),
    path("", projects, name="list_projects"),
]
